/**
 * Routes
 */
export const routes = [
    {
        path: '/',
        redirect: '/Login',
    },
    {
        path: '/Login',
        name: 'Login',
        component: () => import('@/page/TheLogin.vue'),
    },
    {
        path: '/User/:id',
        name: 'User',
        component: () => import('@/page/TheUser.vue'),
    },
]
