import Vue from 'vue'
import App from './App.vue'
import { routes } from './routes/routes'
import VueRouter from 'vue-router'
import store from '@/store'
import axios from 'axios'
import ToastPlugin from 'vue-toast-notification'
import FingerprintJS from '@fingerprintjs/fingerprintjs'
import md5 from 'md5'
import './styles.js'

Vue.config.productionTip = false

FingerprintJS.load()
    .then((fp) => fp.get())
    .then((result) => {
        Vue.prototype.$IMEI = md5(result.visitorId)
    })

Vue.use(VueRouter)
Vue.use(ToastPlugin, {
    position: 'top-right',
    duration: 1700,
})

axios.defaults.headers.common['Content-Type'] = 'application/json'
Vue.prototype.$axios = axios

const router = new VueRouter({
    routes,
})

new Vue({
    render: (h) => h(App),
    router,
    store,
}).$mount('#app')
