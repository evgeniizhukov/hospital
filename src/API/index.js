const URL = {
    DEFAULT: 'https://host1.medsafe.tech:40443/',
    LOGIN: 'https://host1.medsafe.tech:40443/api/client_login',
    TEST: 'https://host1.medsafe.tech:40443/api/test',
}

export default URL
