// mutations
export default {
    setUser(state, user) {
        state.user = { ...user, documents: [] }
    },

    setDocumentInUser(state, documents) {
        state.user.documents = documents
    },
}
